<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

use App\Http\Controllers\UniformController;
use App\Http\Controllers\PerspectiveController;

Route::get('/getUniforms', [UniformController::class, 'getUniforms']);
Route::post('/read', [UniformController::class, 'read']);
Route::post('/save', [UniformController::class, 'save']);

Route::post('/getPerspectives', [PerspectiveController::class, 'getPerspectives']);
