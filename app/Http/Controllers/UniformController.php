<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Uniform;
use App\Models\UniformPart;
use App\Models\Perspective;
use DB;

use Illuminate\Support\Str;
use App\Http\Requests\StoreImage;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Config;

class UniformController extends Controller
{
    public function getUniforms(){
        $uniforms           =   Uniform::getUniforms();
        return response()->json($uniforms);
    }

    public function read(Request $request){
        $uniform            =   Uniform::read($request['uniform_id']);
        $uniform_parts      =   UniformPart::getUniformParts($uniform->id);
        $parts              =   $this->uniformParts($uniform_parts);
        $result             =   [
                                    'uniform'           =>  $uniform,
                                    'uniform_parts'     =>  $parts
                                ];
        return response()->json($result);
    }

    private function uniformParts($uniform_parts){
        $array  =   [];
        foreach ($uniform_parts as $up => $uniform_part) {
            $part               =   new \stdClass();
            $part->part_name    =   $uniform_part->name;
            $part->perspective  =   Perspective::getPerspective($uniform_part->perspective_id)->name;
            if ($uniform_part->image) {
                $part->image          =   app(\App\Http\Controllers\S3ConfigurationController::class)->fetchBucketURL('activity/'.$uniform_part->image);
            }else{
                $part->image          =   '/images/placeholder.png';
            }
            array_push($array, $part);
        }
        return $array;
    }

    public function save(Request $request){
        $result = DB::transaction( function(&$data) use ($request) {
            $data           =   $request->all();
            $validator      =   Validator::make($data, [
                'uniform_name'      => 'required'
            ]);

            if ($validator->fails()) {
                return false;
            }

            $uniform_id     =   Uniform::saveUniform($data['uniform_name']);
            $uniform_parts  =   json_decode($data['uniform_parts']);

            foreach ($uniform_parts as $up => $uniform_part) {
                if ($uniform_part->part_name) {
                    $part_array     =   [
                                            'uniform_id'        =>  $uniform_id,
                                            'name'              =>  $uniform_part->part_name,
                                            'perspective_id'    =>  $uniform_part->perspective->id,
                                            'image'             =>  $uniform_part->image,
                                            'created_at'        =>  date('Y-m-d H:i:s')
                                        ];
                    UniformPart::saveUniformPart($part_array);
                }
            }

            foreach ($request->file() as $f => $file) {
                $image_name     =   $file->getClientOriginalName();
                $path           =   Storage::disk('s3')->put('/activity/'.$image_name, file_get_contents($file), 'public');
            }
            
            return true;
        });       
        return response()->json($result);
    }
}