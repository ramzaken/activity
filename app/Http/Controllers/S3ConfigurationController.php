<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Config;

class S3ConfigurationController extends Controller
{
    public function fetchBucketURL($path){
        $client         =   Storage::disk('s3')->getDriver()->getAdapter()->getClient();

        $bucket         =   Config::get('filesystems.disks.s3.bucket');

        $command = $client->getCommand('GetObject', [
            'Bucket'    =>  $bucket,
            'Key'       =>  $path 
        ]);

        $file           =   $client->createPresignedRequest($command, '+20 minutes');

        $result         =   (string)$file->getUri();

        return $result;
    }

}