<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Perspective;

class PerspectiveController extends Controller
{
    public function getPerspectives(Request $request){
        $data               =   $request->all();
        if (isset($data['query'])) {
            $perspectives   =   Perspective::getPerspectivesQuery($request->input('query'));
        }else{
            $perspectives   =   Perspective::getPerspectives();
        }
        return response()->json($perspectives);
    }
}