<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class UniformPart extends Model
{
    // use HasFactory;
    public static function getUniformParts($uniform_id){
        return DB::table('uniform_parts')
                    ->where('uniform_id', $uniform_id)
                    ->get();
    }

    public static function saveUniformPart($part){
        return DB::table('uniform_parts')
                    ->insert($part);
    }
}
