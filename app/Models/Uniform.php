<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Uniform extends Model
{
    // use HasFactory;
    public static function getUniforms(){
        return DB::table('uniforms as u')
                    ->select(DB::raw('
                        u.id,
                        MAX(u.name) as name,
                        COUNT(up.id) as count 
                    '))
                    ->leftJoin('uniform_parts as up', 'up.uniform_id', '=', 'u.id')
                    ->groupBy('u.id')
                    ->orderBy('u.id', 'desc')
                    ->get();
    }

    public static function read($uniform_id){
        return DB::table('uniforms')
                    ->where('id', $uniform_id)
                    ->first();
    }

    public static function saveUniform($name){
        DB::table('uniforms')
            ->insert([
                'name'  =>  $name
            ]);
        return DB::getPdo()->lastInsertId();
    }
}
