<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Perspective extends Model
{
    // use HasFactory;
    public static function getPerspectives(){
        return DB::table('perspectives')
                    ->get();
    }

    public static function getPerspectivesQuery($query){
        return DB::table('perspectives')
                    ->where('name', 'LIKE', "%{$query}%")
                    ->get();
    }

    public static function getPerspective($perspective_id){
        return DB::table('perspectives')
                    ->where('id', $perspective_id)
                    ->first();
    }
}
