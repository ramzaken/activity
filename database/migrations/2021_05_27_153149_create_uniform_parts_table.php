<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniformPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uniform_parts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('uniform_id');
            $table->string('name', 100);
            $table->unsignedBigInteger('perspective_id');
            $table->text('image');
            $table->timestamps();
            
            $table->index(['perspective_id','uniform_id'],'uniform_parts_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uniform_parts');
    }
}
