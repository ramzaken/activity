<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PerspectiveTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perspectives       =   [
                                    [
                                        'name'          =>  'Front',
                                        'created_at'    =>  date('Y-m-d H:i:s')
                                    ],
                                    [
                                        'name'          =>  'Back',
                                        'created_at'    =>  date('Y-m-d H:i:s')
                                    ],
                                    [
                                        'name'          =>  'Left',
                                        'created_at'    =>  date('Y-m-d H:i:s')
                                    ],
                                    [
                                        'name'          =>  'Right',
                                        'created_at'    =>  date('Y-m-d H:i:s')
                                    ]
                                ];
                                
        foreach ($perspectives as $p => $perspective) {
            $count  =   DB::table('perspectives')
                            ->where('name', $perspective['name'])
                            ->count();
            if ($count == 0) {
                DB::table('perspectives')
                    ->insert([
                        'name'          =>  $perspective['name'],
                        'created_at'    =>  $perspective['created_at']
                    ]);
            }
        }
    }
}
