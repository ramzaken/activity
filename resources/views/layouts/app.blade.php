<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Activity</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{ asset('font-awesome/css/all.css') }}" rel="stylesheet">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <script src="{{ asset('font-awesome/js/fontawesome.min.js') }}"></script>
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
        
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/sweetalert.js') }}"></script>
    </body>
</html>
