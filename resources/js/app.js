require('./bootstrap');

import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import { BootstrapVue, IconsPlugin, LayoutPlugin } from 'bootstrap-vue'

import App from './views/App.vue'

const Index = Vue.component('Index', (resolve) => {
  import(/* webpackChunkName: "Index" */'./views/Index.vue')
    .then((Index) => {
      resolve(Index.default);
    });
});

const Create = Vue.component('Create', (resolve) => {
  import(/* webpackChunkName: "Create" */'./views/Create.vue')
    .then((Create) => {
      resolve(Create.default);
    });
});

const Read = Vue.component('Read', (resolve) => {
  import(/* webpackChunkName: "Read" */'./views/Read.vue')
    .then((Read) => {
      resolve(Read.default);
    });
});

//page not found
const PageNotFound = Vue.component('PageNotFound', (resolve) => {
  import(/* webpackChunkName: "PageNotFound" */'./error/PageNotFound.vue')
    .then((PageNotFound) => {
      resolve(PageNotFound.default);
    });
});

//vuex
Vue.use(Vuex)
//vue router
Vue.use(VueRouter)
//BootstrapVue
Vue.use(BootstrapVue)
//loading template
Vue.component('loading',{ template: '<div class="loader">Loading...</div>'})

const router = new VueRouter({
    mode: 'history',
    routes: [
    	{
            path: '/',
            name: 'index',
            component: Index ,
        },
        {
            path: '/create',
            name: 'create',
            component: Create ,
        },
        {
            path: '/read',
            name: 'read',
            component: Read ,
        },
        {
            path: '/*',
            name: 'page_not_found',
            component: PageNotFound,
        },
    ],
});

//my libraries

//global ajax with access token
Vue.prototype.$sweetAlertAjaxToken = function(message, post_data, method, url, success_callback, error_callback)
{
    swal({
        title: "Confirm Action",
        text: message,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willSave) => {
        if (willSave) {
            $.ajax({
                type: method,
                url: url,
                data: post_data,
                cache: false,
        		contentType: false,
        		processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend : function() {
                    var span = document.createElement("span");
                    span.innerHTML = '<span class="loading-animation">LOADING...</span>';
                    swal({
                        content: span,
                        icon: "warning",
                        buttons: false,
                        closeOnClickOutside: false
                    });
                },
                success: function (data) {
                    success_callback(data);
                },
                error: function (data) {
                    error_callback(data);
                },
                complete : function() {

                }
            });
        } else {
            swal("Aborted!");
        }
    });
}

Vue.prototype.$globalAjaxGet = function(method, url, success_callback, error_callback)
{
    $.ajax({
        type: method,
        url: url,
        cache: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (data) {
            success_callback(data);
        },
        error: function (data) {
            error_callback(data);
        },
    });
}

Vue.prototype.$globalAjaxJwtToken = function(post_data, method, url, success_callback, error_callback)
{
    $.ajax({
        type: method,
        url: url,
        data: post_data,
        cache: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            "Content-Type": "application/x-www-form-urlencoded"
        },
        beforeSend : function() {

        },
        success: function (data) {
            success_callback(data);
        },
        error: function (data) {
            error_callback(data);
        },
        complete : function() {

        }
    });
}


const app = new Vue({
    el: '#app',
    data: { loading: false },
    components: { App },
    router,
});

router.beforeResolve((to, from, next) => {
    if (to.name) {
        app.loading = true
    }
    next()
})

router.afterEach((to, from) => {
    setTimeout(() => app.loading = false, 1000)
})

Vue.config.devtools = true
Vue.config.debug = true
Vue.config.silent = true